---

- name: install Postfix
  package:
    name: "{{ item }}"
    state: present
  with_items:
    - postfix

- name: "Retrieve postfix version #1"
  shell: |
    set -o pipefail
    postconf mail_version | grep "^mail_version = " | cut -d '=' -f 2 | sed -r 's/ +//'
  args:
    executable: /bin/bash
  changed_when: False
  failed_when: "postfix_version_raw is failed and not (ansible_check_mode and 'postconf: No such file or directory' in postfix_version_raw.stderr)"
  register: postfix_version_raw
  check_mode: no

- name: "Retrieve postfix version #2"
  set_fact:
    postfix_version: "{{ postfix_version_raw.stdout }}"

- name: "Retrieve postfix version #3"
  debug:
    var: postfix_version

- name: fail when postfix_version variable is empty
  fail:
  when: not postfix_version

# maps support was split in separate packages in Fedora
- name: install Postfix Maps
  package:
    name:
      - postfix-pcre
    state: present
  when: ansible_distribution == 'Fedora' or (ansible_os_family == "RedHat" and ansible_distribution_major_version|int >= 8)
  notify: restart mail system

- name: Configure TLS
  include_tasks: "tls.yml"
  when: tls_method is defined

- name: Configure connection to Spamassassin
  include_tasks: "spamassassin.yml"
  when: with_spamassassin

- name: Configure connection to PostSRSd
  include_tasks: "postsrsd.yml"
  when: with_postsrsd

- name: install main configuration
  template:
    src: main.cf.j2
    dest: /etc/postfix/main.cf
    owner: root
    group: root
    mode: 0644
  notify: reload mail system

- name: install master configuration
  template:
    src: master.cf.j2
    dest: /etc/postfix/master.cf
    owner: root
    group: root
    mode: 0644
  notify: restart mail system

- name: generate aliases for redirections
  template:
    src: aliases.j2
    dest: /etc/aliases
    owner: root
    group: root
    mode: 0644
  notify: reload mail aliases databases

- name: generate aliases for local accounts
  template:
    src: aliases.local.j2
    dest: /etc/postfix/aliases.local
    owner: root
    group: root
    mode: 0644
  notify: reload mail aliases databases

- name: "Ganerate TLS Policy Exception Map"
  template:
    src: tls_policy_maps
    dest: /etc/postfix/
    owner: root
    group: root
    mode: 0644
  notify: reload mail aliases databases

# IPv6 addresses are more often considered spammy by GMail
- name: "Add transport to redirect all GMail outgoing traffic to IPv4"
  lineinfile:
    path: /etc/postfix/transport
    line: "gmail.com   smtp-ipv4:"
    regex: "^gmail.com   smtp-ipv4:"
  notify: restart mail system

- name: generate mailboxes for local accounts
  file:
    path: "/var/mail/{{ item }}"
    state: directory
    owner: nobody
    group: mail
    mode: 0770
  with_items: "{{ local_accounts }}"

- name: install SASL configuration
  copy:
    src: smtpd.conf
    dest: /etc/sasl2/smtpd.conf
    owner: root
    group: root
    mode: 0644
  when: auth is defined and auth == "sasldb"
  notify: reload mail system

# RedHat-specific paths
# on Debian the various MTA conflicts
- name: setup mta alternatives
  alternatives:
    name: mta
    link: /usr/sbin/sendmail
    path: /usr/sbin/sendmail.postfix
  when: ansible_os_family == "RedHat"

- name: Start service
  service:
    name: postfix
    enabled: yes
    state: started

- name: Setup firewall
  include_tasks: firewall.yml
  when: manage_firewall

